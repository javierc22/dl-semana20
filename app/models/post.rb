class Post < ApplicationRecord
  belongs_to :user
  # "dependent: :destroy" => Forzar a que se eliminen los comentarios cuando el "post" es eliminado.
  has_many :comments, dependent: :destroy
end

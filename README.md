## Semana 21 - CanCanCan

1. Intro a manejo de accesos
2. Guardando los roles en la base de datos
3. Intro a CanCanCan y descarga del proyecto
4. Revisión del proyecto base
5. Seed
6. Habilidades
7. Revisión de habilidades en las vistas
8. Revisión de habilidad en los controllers
9. Resumen de lo visto
10. Excepciones, rescates y bloqueos
11. Test del acceso al borrado
12. Roles y enums

Documentación:

https://github.com/CanCanCommunity/cancancan
https://github.com/CanCanCommunity/cancancan/wiki